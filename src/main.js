import express from 'express';
import calc from './calc.js';

const app = express();
const PORT = 3000;
const HOST = "localhost";

// Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
    res.status(200).send("Hello World!");
});

// Endpoint - Get http://localhost:3000/add?a=2&b=3
app.get('/add', (req,res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const sum = calc.add(a, b);
    res.status(200).send(sum.toString());
}),

app.listen(PORT, HOST, () => {
    console.log(`Listening on http://${HOST}:${PORT}`)
});
