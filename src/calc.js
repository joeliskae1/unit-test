// Add
/**
 * Adds two numbers together. A + B.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

// subtract
/**
 * Subracts two numbers. A - B.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const subtract = (a, b) => a - b;

// multiply
/**
 * Multiplies two numbers. A * B.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant
};

// divide
/**
 * Divides two numbers. A / B.
 * @param {number} dividend 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
        const fraction = dividend / divisor;
        return fraction;
}

export default { add, subtract, multiply, divide }